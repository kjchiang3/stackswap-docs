---
description: High-level description of Stackswap, the environment, and our goals.
---

# Introduction

Stackswap is a decentralized exchange (DEX) that issues and exchanges tokens on the Bitcoin Network through the Stacks blockchain. We aim to create a next-generation DEX to support the future growth of DeFi projects in Bitcoin.

![](.gitbook/assets/image.png)

## Features on Stackswap

#### Swap

Swap your tokens in pools with our automated market maker (AMM).

#### Pool

Pool your tokens to earn a percentage of total trading volume, as well as LP tokens.

#### Token Incentives

Lock in your LP tokens for extra rewards paid in STSW.

#### Stake

Stake your STSW tokens for governance vSTSW tokens, as well as additional STSW rewards.

#### Vault

Create a vault to loan LBTC with STSW or STX as collateral.

#### NFTs

Mint NFTs generated by our AI deep-learning machine.

#### Token Launchpad

Create your project's own token, pool, and get it listed on Stackswap.

## Current problems

### **Untapped Bitcoin liquidity for DeFi**

Bitcoin's market cap stands at around $1.3 trillion, a considerable amount when compared to that of Ethereum ($574 billion). While Ethereum has the upper hand in terms of functions such as smart contract usability, Bitcoin still remains the powerhouse in terms of total value. This is compounded when users realize that Bitcoin has only [1.62% of its total supply utilized](https://defipulse.com/btc) within DeFi markets. That leaves a total amount of over $1.14 trillion that has been left untouched by users.

By creating a marketplace specifically catered to Bitcoin users, and building the foundations for which the Bitcoin DeFi market can take off, Stackswap can create the environment of the Uniswap of Bitcoin. Taking advantage of technologies such as the Stacks blockchain and its PoX (proof-of-transfer) consensus mechanism, Stackswap aims to begin the journey of finally tapping into the dormant majority of Bitcoin liquidity.

### **DeFi can be challenging to enter as a user**

With a field that is as rapidly growing as DeFi, keeping up with information is difficult even for crypto natives. For new users, even those that experienced, that barrier is significantly larger and can be scary. New users, on top of looking out for scams, have to come up to speed with an overwhelming amount of information. This information is usually highly technical, and can block out users who lack programming skills beyond the basic level. Even for experienced users, executing transactions can be time consuming and repetitive, which can potentially lead to expensive mistakes. Blockchain engineers can be hired, but that does not make sense for an individual. Stackswap can act as the individual users' blockchain engineer, with more direction given by the community with the use of the governance program.

### **Lack of drive for projects of lasting value**

Currently, there is little motivation to create lasting value with a blockchain projects. The current landscape of DeFi , dominated by speculators, creates an environment where projects are quick to build up excitement and profited off by price gougers. This also leads to more coin scams to enter the market as a means to quickly build value before a rug pull is executed to the benefit of the scammers. In a 6 month period up to May 2021, 7,000 people in the US reported losses of [more than $80 million through scams](https://www.ftc.gov/news-events/blogs/data-spotlight/2021/05/cryptocurrency-buzz-drives-record-investment-scam-losses#end2).

Ultimately, it leads to a riskier market, opening up DeFi to many of the criticisms crypto-skeptics use as a means to dismiss DeFi as a whole. While this is an expensive learning experience for some users, we believe it is a much more sensible plan to steer the DeFi market towards a direction that rewards good projects that benefits their communities.

### **DeFi users are exposed to high risks**

A market like DeFi that is in the development stage also means there is a lack of analysis tools that benefit the mature traditional finance market. While there are a products in the DeFi space that give the user metrics and other quantitative analysis, there still remains a large gap between what the typical DeFi user and a traditional finance user can understand on a deep level. While traditional finance has various metrics (ROI, IRR, margin, efficiency, etc.) that can give an user a quick idea of how a project is performing, that is much more difficult within DeFi. An overall lack of stability in DeFi gives way to many potentially exciting opportunities with large rates of growth, but is also balanced out by a higher uncertainty and risk profile.

## Solutions by Stackswap

### **Building a Bitcoin-native DEX with the Stacks blockchain**

Stacks operates on Bitcoin and is best placed to take advantage of the Bitcoin market. Bitcoin is the world's biggest cryptocurrency in terms of market capitalization, but is not supported by smart contracts.

By building a Layer-2 protocol on top of Bitcoin, smart contracts are finally usable with Bitcoin, with STX being one of them. This way, instead of competing with Bitcoin and other large protocols, Stacks enables us to take advantage of Bitcoin's capital, security, and network. Building a protocol like Stacks on Bitcoin enables apps and smart contracts to be built while still securing the existing advantages of Bitcoin.

Stacks also employs the Proof-of-Transfer (PoX) consensus mechanism, which is highly advantageous compared to other consensus mechanisms. With this in mind, Stackswap believes that Stacks is the first step to unlocking the potential of the Bitcoin market for the DeFi world.

### **Simplified GUI that can save time and energy without cutting capabilities**

We believe in a simplified user interface that also doesn't limit users from using the most important functions that a DEX offers. Swaps and liquidity pools are the basis of any DEX, and with Stackswap's utilization of the Stacks blockchain, more functions are required. Stacking and mining are key components of the Stacks blockchain's PoX consensus mechanism, as well as farming and staking across many projects throughout the DeFi market. These can be difficult functions for new users to understand and utilize, and it is important that the process is simplified for them to be successful users in blockchain technology. We are well positioned to also support the NFT markets, and plan on using NFTs within our own project for users to gain access to some of our premium features.

Saving users time and energy in not only researching, but understanding technology, is an important goal for us. By doing the work for our users, we can provide a fast and simple user experience.

### **Providing blockchain entrepreneurs resources to create lasting value**

Traditionally, DEXs only focused on speculators- quick to build up excitement, but not lasting value. DEX 2.0 can be established to build lasting value for users, projects, and ecosystems. We can achieve the goal of lasting value by focusing on the other side of the capital market: value builders and matching players (entrepreneurs) who are genuinely interested in creating value.

Those who use our Token Launchpad are most likely to be early stage entrepreneurs. These entrepreneurs have the problem of lacking resources - be it funding, experience, or people - but they have the will to carry things out.

Stackswap's Token Launchpad can help these entrepreneurs get funding, and with enough promise and interest, can also match them with offline or online accelerators and funds, so they can receive extra help and advice from professionals experienced in developing and growing early stage businesses. This can lower the overall risk profile of these projects, bringing extra value to users. Additionally, speculators can also benefit from this exchange of funds, experience, and ideas.

### Use of cutting-edge time-series forecast AI algorithms to mitigate risks

No matter the level of experience of an user, risk is a large factor they must consider. Stackswap's core team has the collective experience of 20 years in artificial intelligence, has spent the past few years verifying their algorithms on the Defi market. Our cutting-edge time-series forecast algorithms derived by Artificial Intelligence have produced unmatched results in the field, showing more than 60% accuracy consistently over past year in predicting price movement directions over short time period up to 7 days.

The key differentiating factors of our algorithm is the fact that it is created and improved by AI and not by human beings. The market dynamics could change drastically in a short period of time, rending any fixed algorithm to not being able to keep up the performance. We believe only by automating the process of algorithm discovery, one can respond to ever-changing dynamics of Defi market landscape and stay successful over a prolonged period of time.

## Our objectives

* Creating a wide-ranging set of functionalities for user participation on the Bitcoin network.
* A simplified dashboard for users to quickly, safely, and successfully execute swaps, liquidity pooling, token launches and more.
* Token launchpads for projects to get a quick start.
* A trustless and permission-less governance system built on the Stackswap token (STSW)
* Support for the Bitcoin DeFi ecosystem and any projects launching on Bitcoin, cultivating the overall growth of DeFi on Bitcoin (and Stackswap!)
