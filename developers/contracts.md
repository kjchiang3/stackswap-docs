---
description: A list of contracts deployed by the Stackswap platform
---

# Contracts

All contracts are built on Stacks, and can be seen on [Stacks Explorer](https://explorer.stacks.co/?chain=mainnet).

| Contract Name                   | Contract Address                                                      |
| ------------------------------- | --------------------------------------------------------------------- |
| Liquidity Token                 | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.liquidity-token-trait-v4c   |
| Restricted Token                | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.restricted-token-trait-v1a  |
| Initializable Token             | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.initializable-trait-v1b     |
| Swap                            | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.stackswap-swap-v5k          |
| One-Step Mint (Token Launchpad) | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.stackswap-one-step-mint-v5k |
| DAO                             | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.stackswap-dao-v5k           |
| Governance                      | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.stackswap-governance-v5k    |
| Farming                         | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.stackswap-farming-v1l       |
| Staking                         | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.stackswap-staking-v1l       |
| vSTSW                           | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.vstsw-token-v1k             |
| STSW                            | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.stsw-token-v4a              |
| wSTX                            | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.wstx-token-v4a              |
| Liquidity Token                 | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.liquidity-token-v5k         |

