# Contract Audit

Our first smart contract audit was completed by Tintash over a period of 3 weeks, completed on November 10th, 2021. A copy of the report can be downloaded below:

{% file src="../.gitbook/assets/Tintash - Stackswap Contract audit.pdf" %}
