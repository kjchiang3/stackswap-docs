# API

{% swagger method="get" path="/api/v1/pools" baseUrl="app.stackswap.org" summary="Get Pool Data" %}
{% swagger-description %}
This endpoint allows you to get active liquidity pool data.
{% endswagger-description %}

{% swagger-parameter in="path" name="identifier*" type="string" required="true" %}
ID of the liquidity pool pair
{% endswagger-parameter %}

{% swagger-parameter in="query" name="token_x_addr" type="string" %}
Contract address of Token X in token pair.
{% endswagger-parameter %}

{% swagger-parameter in="query" name="token_y_addr" type="string" %}
Contract address of Token Y in token pair.
{% endswagger-parameter %}

{% swagger-parameter in="query" name="liquidity_token_addr" type="string" %}
Contract address of liquidity token.
{% endswagger-parameter %}

{% swagger-parameter in="query" name="liquidity_locked" type="string" %}
Amount of liquidity locked in token pair, calculated in USD.
{% endswagger-parameter %}

{% swagger-parameter in="query" name="apy" type="string" %}
APY for token pair LP token farming.
{% endswagger-parameter %}
{% endswagger %}
