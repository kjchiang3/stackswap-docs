---
description: Usages where we believe we can apply our knowledge of AI in DEXs
---

# Use Cases for AI

## DeFi and DEXs

Stackswap believes that we can apply our AI knowledge to increase the value of the Stackswap platform, but some common questions we get are:

* Where does AI fit into DeFi and DEXs?
* Why AI and DEX?
* What role would AI play?

### Time series forecast algorithm

Incorporating time series forecast algorithms to predict market movements is possible, and our algorithm is constantly learning, adapting, and improving. We have the knowledge to provide market forecasts to a limited number of member of our community who put a significant amount of liquidity into the market, which in turn brings more stability to our platform.

This also provides these liquidity providers an advantage by having access to our market forecast, receiving extra benefit from using Stackswap, and not any other DEX.

We will be the very first DEX to provide market forecasts, but will need to control the dissemination of the forecast as it is precious information. So, we will share liquidity providers who share the same vision in creating lasting value for users and the ecosystem as a whole.

### Governance

As blockchain technology and Stackswap's AI matures, we will gradually introduce automation into governance. For example, if transaction fees is found to be one of the significant factors affecting the transaction volume and TVL on the Stackswap platform, an AI could be deployed to optimize the transaction fee to increase the overall value of the platform, rather than repeated voting processes that may result in overcorrections, errors, and lack of timely change. This could elevate Stackswap and DEXs beyond the performance of the traditional minimax optimization algorithms.
