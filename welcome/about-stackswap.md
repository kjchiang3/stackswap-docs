# About Stackswap

## The Story

Three Ph.D. labmates originally leading research in Robotics and Human Computer Interaction (HCI), found success in creating successfull Artificial Intelligence models that transcended widely known methods such as machine learning and neural networks.

After experiencing rug pulls and widespread lack of trust in the early days of investing in crypto, the Founding Team set out to create a better environment that could reduce risk and create value for common investors in crypto.

\[image]

## Stackswap Values

Our broad vision is to create an environment where projects that add value are given a spotlight and room to grow, and users who are on the Stackswap platform are aware of this value-add environment and culture.

**We believe in the following ideals:**

* Permissionless
* Trustless
* Decentralized
* Community-driven

**Our goals as a project are:**

* Creating a wide-ranging set of functionalities for user participation on the Bitcoin network.
* A simplified dashboard for users to quickly, safely, and successfully execute swaps, liquidity pooling, token launches and more.
* Token launchpads for projects to get a quick start.
* A trustless and permission-less governance system.
* Support for the Bitcoin DeFi ecosystem and any projects launching on Bitcoin, cultivating the overall growth of DeFi on Bitcoin (and Stackswap!)
