---
description: A guide to DeFi and more on the Stacks Ecosystem
---

# Guide to Stacks

For information on Stacks, check out the [Stacks Docs.](https://docs.stacks.co/understand-stacks/overview)

### What is Stacks?

Stacks is a Layer-1 solution that aims to expand the Bitcoin ecosystem by securing connectivity between smart contracts and Bitcoin, and to build a blockchain-based Web 3.0.

Stacks employs Proof-of-Transfer (PoX), a consensus algorithm between the two blockchains, to connect Bitcoin and the Stacks blockchain, and extend the functionality of Bitcoin. Stacks (STX) is used as fuel when executing smart contracts through the PoX algorithm.

Bitcoin had no function other than a means of storing value, but through Stacks can have various scalabilities such as DeFi, NFT issuance, and digital asset registration in the blockchain ecosystem.

### PoX: Proof of Transfer Mining with Bitcoin

| Name              | Acronym | Miner action to mint new cryptocurrency                                                    |
| ----------------- | ------- | ------------------------------------------------------------------------------------------ |
| Proof-of-Work     | PoW     | Consumer electricity towards computations to mint units of a new cryptocurrency.           |
| Proof-of-Stake    | PoS     | Dedicate economic stake in a base cryptocurrency to mint units of the same cryptocurrency. |
| Proof-of-Burn     | PoB     | Destroy a base cryptocurrency to mint units of a new cryptocurrency.                       |
| Proof-of-Transfer | PoX     | Transfer a base cryptocurrency to mint units of a new cryptocurrency.                      |

PoX mining uses the Bitcoin blockchain as the base cryptocurrency. While any proof-of-work cryptocurrency may be used, Stackswap believes that Bitcoin is the best choice as it is undoubtedly the most secure PoW blockchain. While this consensus may not be as quick as other mechanisms, it retains a high level of reliability adopted from the Bitcoin chain.

### What is Stacking?

STX holders can lock up their STX tokens to participate in the PoX consensus mechanism and receive Bitcoin as rewards. The process is called stacking. The Bitcoin reward rate is determined by various factors, and STX holders run a full node to participate, lock up their STX, and obtain bitcoin.

Stacks chose Bitcoin as the blockchain to power consensus for many reasons. BTC has held the highest market capitalization of any cryptocurrency for the past decade. Bitcoin champions simplicity and stability, and has stood the test of time. Influencing or attacking the network is infeasible or impractical for any potential hackers. It's one of the only cryptocurrencies to capture public attention. Bitcoin is a household name, and is recognized as an asset by governments, large corporations, and legacy banking institutions. Lastly, Bitcoin is largely considered a reliable store of value, and provides extensive infrastructure to support the proof-of-transfer consensus mechanism.

### Getting Started

STX is the native cryptocurrency of the Stacks network. It's used as fuel for blockchain transactions and enables holder to earn Bitcoin with Stacking. STX can be acquired through the following sources:

* [Binance](https://www.binance.com)
* [Okcoin](https://www.okcoin.com)
* [Blockchain.com](https://www.blockchain.com)
* [KuCoin](https://www.kucoin.com)
* [Crypto.com](https://crypto.com)
* [Upbit](https://upbit.com/home)
* [OKEx](https://www.okex.com)
* [7b](https://sevenb.io)
* [Tokocrypto](https://www.tokocrypto.com)
* [FTX](https://ftx.com)

### Set up Hiro Wallet

Currently, we utilize [Hiro Wallet](https://www.hiro.so/wallet) for synergy with the Stacks ecosystem. Follow instructions [here](https://www.hiro.so/wallet) to set up a wallet and install the web extension required to use the Stackswap platform. Then, simply click “Connect to a wallet” on the top right of the [Stackswap](https://app.stackswap.org) page and select a wallet on the extension’s pop-up window.
