# Table of contents

## 👋 Welcome

* [Introduction](README.md)
* [About Stackswap](welcome/about-stackswap.md)
* [Guide to Stacks](welcome/guide-to-stacks.md)

## 📖 Stackswap Platform

* [Platform Features](stackswap-platform/platform-features.md)
* [Use Cases](stackswap-platform/use-cases.md)
* [Technical Architecture](stackswap-platform/technical-architecture.md)
* [User Guides](stackswap-platform/user-guides/README.md)
  * [Swap](stackswap-platform/user-guides/swap.md)
  * [Pool](stackswap-platform/user-guides/pool.md)
  * [Token Incentives](stackswap-platform/user-guides/token-incentives.md)
  * [Staking](stackswap-platform/user-guides/staking.md)
* [FAQ](stackswap-platform/faq.md)
* [Error Codes](stackswap-platform/error-codes.md)

## 🏭 The Project

* [Tokenomics](the-project/tokenomics.md)
* [Governance](the-project/governance.md)
* [Investors](the-project/investors.md)

## ✍ Litepaper and Longform

* [Stackswap Litepaper](litepaper-and-longform/stackswap-litepaper.md)
* [Use Cases for AI](litepaper-and-longform/use-cases-for-ai.md)

## 👩💻 Developers

* [Contracts](developers/contracts.md)
* [Github](developers/github.md)
* [API](developers/api.md)
* [Contract Audit](developers/contract-audit.md)
* [Bug Bounty](developers/bug-bounty.md)

## 🫂 Community

* [Stackswap Links](community/stackswap-links.md)
* [Brand Assets](community/brand-assets.md)
