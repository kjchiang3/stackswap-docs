# Error Codes

General

|    |                      |
| -- | -------------------- |
| u1 | Insufficient balance |

Swap and Pool

|       |                                 |
| ----- | ------------------------------- |
| u4161 | Not owner of required token     |
| u4163 | Pair already exists             |
| u4165 | Slippage passed tolerance level |
| u4168 | Value out of range              |
| u4169 | No fee for token X              |
| u4170 | No fee for token Y              |
| u4174 | Safe transfer amount            |
| u4175 | Safe burn amount                |
| u4176 | Safe mint amount                |

Farm

|       |                            |
| ----- | -------------------------- |
| u4300 | Staking not available      |
| u4301 | Cannot stake               |
| u4302 | Insufficient balance       |
| u4303 | Reward cycle not completed |
| u4304 | Nothing to redeem          |
| u4305 | Permission denied          |
| u4309 | Invalid wSTX token         |
| u4311 | Transfer fail              |
| u4312 | Pool not enrolled          |
| u4313 | Token already in pool      |
| u4314 | Farm ended                 |
| u4315 | Farm not ended             |

Stake

|       |                            |
| ----- | -------------------------- |
| u4330 | Staking not available      |
| u4331 | Cannot stake               |
| u4333 | Reward cycle not completed |
| u4334 | Nothing to redeem          |
| u4338 | Transfer fail              |
| u4339 | Stake ended                |
| u4341 | Not authorized             |

One-Step Mint

|       |                    |
| ----- | ------------------ |
| u4003 | DAO access         |
| u4004 | LP Token not valid |
