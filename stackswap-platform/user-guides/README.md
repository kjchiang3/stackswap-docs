# User Guides

Here you can find the user guides to all the functions available on Stackswap. The user guides will be added to and updated with more feedback from users.

For other questions, reach out to our community and team on [Discord](https://discord.gg/HUv6eChp9p).

### User guide directory

| Page Name        | Link                  |
| ---------------- | --------------------- |
| Swap             | [Swap Guide](swap.md) |
| Pool             | [Pool Guide](pool.md) |
| Token Incentives | Coming soon           |
| Staking          | Coming soon           |
| PoXL Mining      | Coming soon           |
| Token Launchpad  | Coming soon           |
