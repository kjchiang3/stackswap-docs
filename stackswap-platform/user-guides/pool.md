# Pool

## How do I add liquidity to a pool?

To join a pool, you must have both tokens in the pool in your wallet. For example, in the pool below, you must have both STX and STSW tokens in your wallet before you can add liquidity to the pool.

![](../../.gitbook/assets/pool1.png)

Then, you can click on "Add" to join in on providing liquidity.

![](../../.gitbook/assets/pool2.png)

The ratio between the two tokens is set on creation of the pool, and changes through swaps that alter the ratio. When you add liquidity to a pool, you will receive LP tokens in return. These tokens can be locked into the \[Farms] for a period of time in exchange for STSW token rewards.

## The pool I want to add liquidity does not exist.

If the pair you want does not exist on the platform, you can create one. Navigate to “[Create Pair](https://app.stackswap.org/pool/create-pair)”.&#x20;

![](../../.gitbook/assets/pool3.png)

Here, you can select the two tokens you would like to create a pool with. The ratio of tokens you add will set the price of this pool. Click “Enter Amount” to review the details before confirming.&#x20;



## How do I add to my liquidity position?&#x20;

From your list of liquidity pools, click on the pool pair to expand the section. From there you can navigate to “Add Liquidity”, where you can input the amount you would like to add to the pool. The price ratios of the token pair will be shown on the bottom, and you can click “Enter Amount” to see full details before confirming the transaction. Don’t forget to check your transaction settings to set your slippage tolerance percentages.&#x20;

## How do I subtract from my liquidity position?&#x20;

From your list of liquidity pools, click on the pool pair to expand the section. From there you can navigate to “Remove Liquidity”. For ease of use, there are both a slider to adjust your removal by percentage or quick buttons to set an amount. Then, you can click “Remove Liquidity” to see full details before confirming the transaction. Don’t forget to check your transaction settings to set your slippage tolerance percentages.
