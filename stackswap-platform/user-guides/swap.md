# Swap

## **Acquire STX**

Before you can swap tokens, you must first have STX in your wallet to swap for other tokens and to pay for transaction fees on the Stacks chain. To see where you can get STX, see the [Stacks site](https://www.stacks.co/explore/get-stx#getstx).

## **How do I execute a swap?**

Select the two tokens you would like to swap in the “From” and “To” fields. Enter the amount you would like to swap in the “From” input field, or use the “Max” button to use the entire balance in your wallet. Wait a few seconds to see the ratio and the expected amount that you should receive in the trade.&#x20;

![](../../.gitbook/assets/swap1.png)

Make sure to check your slippage tolerance settings so you can be guaranteed a swap you will be happy with! Click “Enter Amount” at the bottom to see full details before you “Confirm Swap.”

![](../../.gitbook/assets/swap2.png)
