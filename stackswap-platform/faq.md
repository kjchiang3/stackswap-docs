---
description: FAQs regarding Stackswap and specific Stacks questions.
---

# FAQ

For other questions, reach out to our community and team on [Discord](https://discord.gg/HUv6eChp9p).

### My Stacks transaction has been pending for a while, why are transactions so slow?

Occasionally, especially around big launches of projects, the Stacks blockchain can be congested due to too many transactions happening at once. When this happens, transactions can take a few hours or up to a day. If you wait 1-2 days, the network will gradually stabilize, but if you don't want to wait, you can try making your transaction fee larger to help push your transaction to the top of the list.

### **The button says “Pair not Available”. What should I do?**

If a pair is not available, a pool between the pair does not yet exist. To create a pool, head over to the [Pool tab](https://app.stackswap.org/pool/liquidity). Read more at the [Pool](broken-reference) guide.

### How do I see my current liquidity assets?&#x20;

Once your wallet is connected, the liquidity pools you are in will show up on the screen. If a few seconds goes by and you don’t see ones you are in, you can [import them directly](https://app.stackswap.org/pool/import).&#x20;
