---
description: Who can gain from using Stackswap?
---

# Use Cases

Stackswap aims to establish a DEX that can offer products to a variety of users, for business and individual transactions, as well as projects and teams.

### Individuals

Stackswap can facilitate various peer-to-peer transactions, and can make such transactions simple and easy on the Stackswap app. Individuals can trade within pools with prices set by our AMM mechanism, and can also earn fees from liquidity provision.

### Institutions

Institutions can similarly trade and earn fees from liquidity provision, on a larger scale. They can also use Stackswap as hunting grounds to look for interesting and promising projects to invest in, and get in on the ground floor for such projects.

### Projects and Teams

Projects and their teams can utilize the Stackswap DEX to leverage the DeFi ecosystem even without much technical prowess. They can find the resources they lack-- funding, experience, or personnel-- to kickstart their projects. With more time and as Stackswap creates more partnerships, these projects and teams can get matched with online and offline accelerators and funds.
