---
description: Description of the features and functions on the Stackswap platform
---

# Platform Features

### Swap

Users can exchange Stacks protocol based tokens with each other through swap functions. X token is immediately exchanged for a Y token through the swap function. At this time, the exchange price is determined by the ratio between X and Y tokens in the liquidity pool. There is also an opportunity for arbitrage if token prices for tokens X and Y, and the exchange rate between X and Y tokens of the liquidity pool, differ in price from external channels.

### Pool

The Liquidity Providers (LPs) provide token liquidity to the pool and allow our Swaps to function. LPs supply liquidity to pools and receive LP tokens issued with protocols in return. The LP token is a transferable token that is paid in proportion to the amount that contributes to the liquidity pool. The 0.3% transaction fee for transactions made by the protocol is used to be distributed to LPs in the pool and Stackswap protocol maintenance.

### Token Launchpad

The token launchpad allows non-programmers to quickly enter the Bitcoin blockchain ecosystem by providing an easy-to-use interface to create and list tokens. Not only can users create tokens, they can take advantage of PoXL mining and stacking user interfaces on Stackswap without a single line of code.

### Token Incentives

The liquidity provider receives the reward tokens based on the contribution to the liquidity supply provided. Reward tokens per block are allocated according to the weighted average liquidity value that considers the amount of time the token is held. The annual profit rate (APR) is calculated as:

$$
APR = (Reward * Pool share) / User Capital
$$

* APR: value in percentage
* Reward: block reward per year
* Pool Share: percentage of total LP tokens in pool by user
* User Capital: Capital (in LP tokens) pooled by user



### Staking

STSW token stakers will be rewarded with the vSTSW token, which can be used as proof of staking and can only be used on the Stackswap platform vSTSW token holders can use their tokens as voting power, and will also be rewarded additional STSW tokens as staking rewards. Equal amounts of STSW staked will receive different amounts of voting power (vSTSW) depending on the stake period. Users who lock in their STSW tokens with a longer staking period will receive more vSTSW tokens as reward.

$$
StakingPeriod = DailyBlocks * 30 days
$$

* Staking period: 1 month period (calculated as 30 days)
* Daily blocks: 144 blocks per day

Users can stake their STSW to receive vSTSW tokens, which will be used as proof of staking (similar to LP tokens), and be non-transferrable outside the Stackswap platform. vSTSW token holders will also gain the right to create proposals, vote on important governance proposals, and receive additional STSW tokens as reward. For equal amounts of STSW staked, the longer stake periods will receive higher rewards in the form of vSTSW. The calculations for vSTSW returns for STSW staked are as follows:

| Staking Period | STSW Staked | vSTSW Rewarded | Rewards Multiplier\*\* |
| -------------- | :---------: | -------------- | ---------------------- |
| 1 month        |     100     | 106            | 1.059                  |
| 2 months       |     100     | 112            | 1.122                  |
| 3 months       |     100     | 119            | 1.189                  |
| 4 months       |     100     | 126            | 1.260                  |
| 5 months       |     100     | 133            | 1.335                  |
| 6 months       |     100     | 141            | 1.414                  |
| 7 months       |     100     | 150            | 1.498                  |
| 8 months       |     100     | 159            | 1.587                  |
| 9 months       |     100     | 168            | 1.682                  |
| 10 months      |     100     | 178            | 1.782                  |
| 11 months      |     100     | 189            | 1.888                  |
| 12 months      |     100     | 200            | 2.000                  |
| 24 months      |     100     | 400            | 4.000                  |
| 36 months      |     100     | 800            | 8.000                  |

$$
Multiplier = 2 ^ { n / 12}
$$

Where _n_ is the number of months staked, multiplier rounded to 3 decimal places

**vSTSW Token**

Voting rights are awarded in the form of vSTSW for each event of staking STSW tokens. The staking cannot be cancelled until the end of the staking contract period. At the end of the staking contract period, the user can reclaim their staked STSW to their wallet. In act of reclaiming their staked STSW, the user's vSTSW held will immediately be burned, along with the corresponding votoing rights. This triggers the cooldown operation, which prevents the user from staking their tokens for a period of 1 week. If the user does not reclaim their STSW tokens at the end of the staking contract period, they will retain their vSTSW in perpetuity until they decide to claim their STSW tokens.

### Voting

To participate in governance voting, the user must stake their STSW tokens to the Stackswap protocol. When the user stakes their token in the operation described [above](platform-features.md#stake), they will obtain vSTSW, which represents their right to vote. In essence, vSTSW represents tokenized voting power, but voting power is not granted directly to STSW.

### Market Forecast

No form of 'weather forecasts' exist for the Defi market, yet there is a need for it due to the volatility users are exposed to. To enhance yield and lower risk, the protocol will provide its top-of-the-line prediction data to selected users. Stackswap's Dev team also leverages its expertise in AI by employing its proprietary AI-assisted algorithm to save decision-making time and reduce risks. Under the hood, our cutting-edge AI's filtering system rigorously analyzes the Defi market data in a way that cannot be accomplished with common AI methodologies such as deep learning, and can do it all in real time.&#x20;

The problem with deep learning, for example, is that it is no methodology that responds well to fast-changing market conditions that are common in Defi. The team believes that there are many prevalent issues with the traditional statistical approach to participating in Defi, as such the lack of a powerful enough method to accurately predict outcomes to an adequate level. With years of research accumulated through Ph.D. programs and personal participation in Defi, the Stackswap team has confidence in being able to offer the best performing AI prediction algorithm in the world.
