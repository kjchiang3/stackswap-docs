# Contract Audit

Our first smart contract audit was completed by Tintash over a period of 3 weeks, completed on November 10th, 2021. A copy of the report can be downloaded below:

{% embed url="https://files.gitbook.com/v0/b/gitbook-x-prod.appspot.com/o/spaces%2FR2Qy2jO41SJwWbgnrvP4%2Fuploads%2F9tk4xdtFNupBe79yjKqY%2FTintash%20-%20Stackswap%20Contract%20audit.pdf?alt=media&token=b58b7f73-60a6-45e3-a8a5-b5e6f1bde584" %}

{% file src="../.gitbook/assets/Tintash - Stackswap Contract audit.pdf" %}
