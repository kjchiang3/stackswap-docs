# Governance

Stackswap will employ an on-chain governance system for critical decisions and development direction. The Stackswap community will vote on key decisions, such as the use of Treasury, token minting and burning, and transaction fees. Stackswap will be the decision-making body during the initial phase until the Stackswap community grows to a strong user base.

As blockchain technology and Stackswap's AI matures, Stackswap will gradually introduce automation into governance. For example, if transaction fees is found to be one of the significant factors affecting the transaction volume and TVL on the Stackswap platform, an AI could be deployed to optimize the transaction fee to increase the overall value of the platform, rather than repeated voting processes that may result in overcorrections, errors, and lack of timely change. This could elevate Stackswap beyond the performance of the traditional minimax optimization algorithms.

STSW holders can stake their token after the introduction of Stackswap governance, receiving vSTSW tokens as well as additional STSW tokens as rewards.

### Voting

To participate in governance voting, the user must stake their STSW tokens to the Stackswap protocol. When the user stakes their token in the operation described above, they will obtain vSTSW, which represents their right to vote. In essence, vSTSW represents tokenized voting power, but voting power is not granted directly to STSW.

### vSTSW Token

Voting rights are awarded in the form of vSTSW for each event of staking STSW tokens. The staking cannot be cancelled until the end of the staking contract period. At the end of the staking contract period, the user can reclaim their staked STSW to their wallet. In act of reclaiming their staked STSW, the user's vSTSW held will immediately be burned, along with the corresponding votoing rights. This triggers the cooldown operation, which prevents the user from staking their tokens for a period of 1 week. If the user does not reclaim their STSW tokens at the end of the staking contract period, they will retain their vSTSW in perpetuity until they decide to claim their STSW tokens.



| Staking Period | STSW Staked | vSTSW Rewarded | Rewards Multiplier\*\* |
| -------------- | :---------: | -------------- | ---------------------- |
| 1 month        |     100     | 106            | 1.059                  |
| 2 months       |     100     | 112            | 1.122                  |
| 3 months       |     100     | 119            | 1.189                  |
| 4 months       |     100     | 126            | 1.260                  |
| 5 months       |     100     | 133            | 1.335                  |
| 6 months       |     100     | 141            | 1.414                  |
| 7 months       |     100     | 150            | 1.498                  |
| 8 months       |     100     | 159            | 1.587                  |
| 9 months       |     100     | 168            | 1.682                  |
| 10 months      |     100     | 178            | 1.782                  |
| 11 months      |     100     | 189            | 1.888                  |
| 12 months      |     100     | 200            | 2.000                  |
| 24 months      |     100     | 400            | 4.000                  |
| 36 months      |     100     | 800            | 8.000                  |

$$
Multiplier = 2 ^ { n / 12}
$$

\*\*where _n_ is the number of months staked, multiplier rounded to 3 decimal places

### Staking Mechanisms

STSW token stakers will be rewarded with the vSTSW token, which can be used as proof of staking and can only be used on the Stackswap platform vSTSW token holders can use their tokens as voting power, and will also be rewarded additional STSW tokens as staking rewards. Equal amounts of STSW staked will receive different amounts of voting power (vSTSW) depending on the stake period. Users who lock in their STSW tokens with a longer staking period will receive more vSTSW tokens as reward.

$$
StakingPeriod = DailyBlocks * 30 days
$$

* Staking period: 1 month period (calculated as 30 days)
* Daily blocks: 144 blocks per day

Users can stake their STSW to receive vSTSW tokens, which will be used as proof of staking (similar to LP tokens), and be non-transferrable outside the Stackswap platform. vSTSW token holders will also gain the right to create proposals, vote on important governance proposals, and receive additional STSW tokens as reward. For equal amounts of STSW staked, the longer stake periods will receive higher rewards in the form of vSTSW.

### Voting

To participate in governance voting, the user must stake their STSW tokens to the Stackswap protocol. When the user stakes their token in the operation described above, they will obtain vSTSW, which represents their right to vote. In essence, vSTSW represents tokenized voting power, but voting power is not granted directly to STSW.
