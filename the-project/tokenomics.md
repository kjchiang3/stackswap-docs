# Tokenomics

STSW is a governance token that also rewards its holders with a share of exchange revenues. The token distribution follows a fixed supply, cliff and linear schedule model.

## ​Information

|                      |                                                                                                                                                                                                                                                                                                                                                                                         |
| -------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Token Name           | Stackswap Token                                                                                                                                                                                                                                                                                                                                                                         |
| Token Ticker         | STSW                                                                                                                                                                                                                                                                                                                                                                                    |
| Initial Token Supply | 1,000,000,000 STSW                                                                                                                                                                                                                                                                                                                                                                      |
| Contract Address     | SP1Z92MPDQEWZXW36VX71Q25HKF5K2EPCJ304F275.stsw-token-v4a                                                                                                                                                                                                                                                                                                                                |
| Description          | The native cryptographically-secured fungible protocol token for the decentralized AMM, Token Launchpad, and AI market forecast protocol for Stackswap (ticker symbol STSW) is a transferable representation of attributed governance and utility functions specified in the protocol/code of Stackswap, and is designed to be used solely as an interoperable utility token thereon.   |

## Token Usage

### Governance (voting)

STSW will allow holders to propose and vote on governance proposals to determine future features and/or parameters of Stackswap, the Token Launchpad or Market Forecast with voting weight calculated in proportion to the tokens staked.

The right to vote is restricted solely to voting on features of Stackswap. It does not entitle STSW holders to vote on the operation and management of Stackswap, its affiliates, or their assets or the disposition of such assets to token holders, or select the board of directors of these entities, or determine the development direction of these entities, nor does STSW constitute any equity interest in any of these entities or any collective investment scheme. The arrangement is not intended to be any form of joint venture or partnership.

### Staking

Users may stake their token and earn the right to vote (vSTSW), and receive additional STSW tokens as a reward for creating proposals, voting, and contributing to the platform.

### Token Launchpad

Once the circulation of STSW reaches maturity, STSW must be used to gain access to the Token Launchpad. As the native platform currency, it would also be used to purchase native tokens issued by projects on the Token Launchpad.

### Provide Liquidity

Users can contribute to the protocol by providing their liquidity in the form of STSW.

### Market Forecast

Selected users will be able to receive the AI market forecast feed by holding a minimum of STSW that will be determined.
