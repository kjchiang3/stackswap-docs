---
description: >-
  We have a variety of investors of different backgrounds, who believe in our
  mission and trajectory.
---

# Investors

Stackswap is proud to have highly qualified investors from all around the globe, who are actively involved in the growth, development and success of our project.

![](<../.gitbook/assets/Investors (1)>)
